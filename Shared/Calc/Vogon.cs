﻿using System;
using System.Timers;

namespace Rohrberechnung.Calc
{
    public class Vogon
    {
        public static DataSafe Data;
        public static DataSet CurrentData;
        // private static MainWindow Main;
        private static System.Timers.Timer timer;

        public Vogon() // MainWindow main)
        {
            Data = new DataSafe();
            CurrentData = new DataSet();
            // Main = main;
            timer = new System.Timers.Timer();
            timer.Interval = 4000;
            timer.Elapsed += timer_Tick;
            timer.Stop();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // Main.ResetStatusBar();
            timer.Stop();
        }

        public static void Info(string text)
        {
            // Main.Info(text);
            timer.Start();
        }

        public static void Error(string text)
        {
            // Main.Error(text);
            timer.Start();
        }

        public String[] NewInput(string[] input, int changed, int hold)
        {
            if (!commaPointTest(input))
            {
                DataSet output = Calculate.CalculateMissing(Calculate.StringToDataSet(input), changed, hold);
                if (Data.Count(0) > 0)
                {
                    if (output != Data.Get(0, 0)) Data.Add(0, output);
                }
                else Data.Add(0, output);

                return Calculate.DataSetToString(output);
            }
            else return input;
        }

        private bool commaPointTest(string[] data)
        {
            bool commaPoint = false;
            foreach (string test in data)
            {
                if (test.EndsWith(".") || test.EndsWith(","))
                {
                    commaPoint = true;
                    break;
                }
            }
            return commaPoint;
        }

        #region Test

        public static bool TestAussen()
        {
            return CurrentData.Aussen != 0;
        }

        public static bool TestInnen()
        {
            return CurrentData.Innen != 0;
        }

        public static bool TestWand()
        {
            return CurrentData.Wand != 0;
        }

        public static bool TestLaenge()
        {
            return CurrentData.Laenge != 0;
        }

        public static bool TestGewicht()
        {
            return CurrentData.SpezGewicht != 0;
        }

        public static bool TestLiefermengeKg()
        {
            return CurrentData.LiefermengeKg != 0;
        }

        public static bool TestLiefermengeStk()
        {
            return CurrentData.LiefermengeStk != 0;
        }

        public static bool TestMaterialpreis()
        {
            return CurrentData.Materialpreis != 0;
        }

        public static bool TestBearbeitungspreis()
        {
            return CurrentData.Bearbeitungspreis != 0;
        }


        public static bool TestAussen(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).Aussen != 0;
        }

        public static bool TestInnen(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).Innen != 0;
        }

        public static bool TestWand(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).Wand != 0;
        }

        public static bool TestLaenge(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).Laenge != 0;
        }

        public static bool TestGewicht(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).SpezGewicht != 0;
        }

        public static bool TestLiefermengeKg(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).LiefermengeKg != 0;
        }

        public static bool TestLiefermengeStk(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).LiefermengeStk != 0;
        }

        public static bool TestMaterialpreis(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).Materialpreis != 0;
        }

        public static bool TestBearbeitungspreis(int oldSaved, int index)
        {
            return Data.Get(oldSaved, index).Bearbeitungspreis != 0;
        }

        #endregion

        public static String Umbruch = "\r\n";
    }
}
