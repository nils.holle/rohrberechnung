﻿using System;

namespace Rohrberechnung.Calc
{
    public class Alloy
    {
        private string alloy;
        private double density;

        public String AlloyName
        {
            get { return alloy; }
            set { alloy = value; }
        }

        public Double Density
        {
            get { return density; }
            set { density = value; }
        }
    }
}
