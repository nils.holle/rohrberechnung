﻿using System;
using System.Collections.Generic;

namespace Rohrberechnung.Calc
{
    public class DataSafe
    {
        private List<DataSet> oldData;
        private List<DataSet> savedData;
        public List<DataSet>[] Data;

        internal DataSafe()
        {
            oldData = new List<DataSet>();
            savedData = new List<DataSet>();
            Data = new List<DataSet>[2];
            Data[0] = oldData;
            Data[1] = savedData;
        }

        public void Add(int oldSaved, DataSet set)
        {
            Data[oldSaved].Add(set);
        }

        public void Remove(int oldSaved, int index)
        {
            Data[oldSaved].RemoveAt(index);
        }

        public void RemoveRange(int oldSaved, int start, int count)
        {
            Data[oldSaved].RemoveRange(start, count);
        }

        public void RemoveAll(int oldSaved)
        {
            Data[oldSaved].Clear();
        }

        public DataSet Get(int oldSaved, int index)
        {
            if (oldSaved == 0) return oldData[index];
            else return savedData[index];
        }

        public void Set(int oldSaved, int index, DataSet set)
        {
            if (index < Data[oldSaved].Count)
            {
                if (oldSaved == 0) oldData[index] = set;
                else if (oldSaved == 1) savedData[index] = set;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public Int32 Count(int oldSaved)
        {
            return Data[oldSaved].Count;
        }
    }
}
