﻿using System;
using System.Text;

namespace Rohrberechnung.Calc
{
    public static class DataAsText
    {
        public static String Everything()
        {
            StringBuilder builder = new StringBuilder();

            if (Vogon.TestAussen() && Vogon.TestInnen() && Vogon.TestWand() && Vogon.TestGewicht())
            {
                // Vom Nutzer schon eingegebene, obere Werte (nur so zur Übersicht...)

                // Legierung
                builder.Append(Languages.Get("keyAlloyOutput"));
                builder.Append(Vogon.CurrentData.Legierung + Vogon.Umbruch);

                // Dichte
                builder.Append(Languages.Get("keyDensityOutput"));
                builder.Append(Vogon.CurrentData.SpezGewicht + " g/cm³" + Vogon.Umbruch);

                // Außendurchmesser mit Toleranzen
                builder.Append(Languages.Get("keyOuterDiameterOutput"));
                builder.Append(Vogon.CurrentData.Aussen + " mm   +" + Vogon.CurrentData.TolAussenPlus + "   -" + Vogon.CurrentData.TolAussenMinus + Vogon.Umbruch);

                // Innendurchmesser mit Toleranzen
                builder.Append(Languages.Get("keyInnerDiameterOutput"));
                builder.Append(Vogon.CurrentData.Innen + " mm   +" + Vogon.CurrentData.TolInnenPlus + "   -" + Vogon.CurrentData.TolInnenMinus + Vogon.Umbruch);

                // Wanddicke mit Toleranzen
                builder.Append(Languages.Get("keyWallThicknessOutput"));
                builder.Append(Vogon.CurrentData.Wand + " mm    +" + Vogon.CurrentData.TolWandPlus + "   -" + Vogon.CurrentData.TolWandMinus + Vogon.Umbruch);

                // Platz schaffen
                builder.Append(Vogon.Umbruch);


                // Metergewichte
                builder.Append(Languages.Get("keyWeightperMeterOutput"));
                builder.Append(Calculate.kgProMeter().ToString("0.000000") + " kg/m" + Vogon.Umbruch);
                builder.Append("                     ");
                builder.Append(Calculate.meterProKG().ToString("0.000000") + " m/kg");


                if (Vogon.TestLaenge())
                {
                    // Platz schaffen
                    builder.Append(Vogon.Umbruch + Vogon.Umbruch + Vogon.Umbruch);

                    // Stückgewicht
                    builder.Append(Languages.Get("keyWeightPerPieceOutput"));
                    builder.Append(Calculate.stueckgewichtKgProStueck().ToString("0.000000"));
                    builder.Append(" kg" + Vogon.Umbruch);


                    // Vom Nutzer schon eingegebene, untere Werte (nur so zur Übersicht...)
                    // Länge
                    builder.Append(Vogon.Umbruch);
                    builder.Append(Languages.Get("keyLengthPerPieceOutput"));
                    builder.Append(Vogon.CurrentData.Laenge + " mm");

                    // Liefermenge
                    if (Vogon.TestLiefermengeKg() && Vogon.TestLiefermengeStk())
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        // In kg
                        builder.Append(Languages.Get("keyDeliveryQuantityOutput"));
                        builder.Append(Vogon.CurrentData.LiefermengeKg + " kg" + Vogon.Umbruch);

                        // In Stk.
                        builder.Append("                     ");
                        builder.Append(Vogon.CurrentData.LiefermengeStk);
                        builder.Append(Languages.Get("keyPcsOutput") + Vogon.Umbruch);

                        // In Meter
                        builder.Append("                     ");
                        builder.Append(Calculate.liefermengeMeter().ToString("0.00") + " m");
                    }

                    // Kosten
                    if (Calculate.kostenProStueck() != 0 && Calculate.kostenGesamt() != 0)
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        builder.Append(Languages.Get("keyMaterialCostsOutput"));
                        builder.Append(Vogon.CurrentData.Materialpreis + Languages.Get("keyEuroPerOutput") + " " + Vogon.CurrentData.MaterialpreisEinheit);

                        builder.Append(Vogon.Umbruch);

                        builder.Append(Languages.Get("keyHandlingChargesOutput"));
                        builder.Append(Vogon.CurrentData.Bearbeitungspreis + Languages.Get("keyEuroPerOutput") + " " + Vogon.CurrentData.BearbeitungspreisEinheit);

                        // Platz schaffen
                        builder.Append(Vogon.Umbruch);

                        builder.Append(Languages.Get("keyCostsPerPieceOutput"));
                        builder.Append(Calculate.kostenProStueck().ToString("0.00") + "€" + Vogon.Umbruch);

                        builder.Append(Languages.Get("keyCostsOutput"));
                        builder.Append(Calculate.kostenGesamt().ToString("0.00") + "€");
                    }
                }
            }

            return builder.ToString();
        }

        public static String UserInput()
        {
            StringBuilder builder = new StringBuilder();

            if (Vogon.TestAussen() && Vogon.TestInnen() && Vogon.TestWand() && Vogon.TestGewicht())
            {
                // Vom Nutzer schon eingegebene, obere Werte (nur so zur Übersicht...)

                // Legierung
                builder.Append(Languages.Get("keyAlloyOutput"));
                builder.Append(Vogon.CurrentData.Legierung + Vogon.Umbruch);

                // Dichte
                builder.Append(Languages.Get("keyDensityOutput"));
                builder.Append(Vogon.CurrentData.SpezGewicht + " g/cm³" + Vogon.Umbruch);

                // Außendurchmesser mit Toleranzen
                builder.Append(Languages.Get("keyOuterDiameterOutput"));
                builder.Append(Vogon.CurrentData.Aussen + " mm   +" + Vogon.CurrentData.TolAussenPlus + "   -" + Vogon.CurrentData.TolAussenMinus + Vogon.Umbruch);

                // Innendurchmesser mit Toleranzen
                builder.Append(Languages.Get("keyInnerDiameterOutput"));
                builder.Append(Vogon.CurrentData.Innen + " mm   +" + Vogon.CurrentData.TolInnenPlus + "   -" + Vogon.CurrentData.TolInnenMinus + Vogon.Umbruch);

                // Wanddicke mit Toleranzen
                builder.Append(Languages.Get("keyWallThicknessOutput"));
                builder.Append(Vogon.CurrentData.Wand + " mm    +" + Vogon.CurrentData.TolWandPlus + "   -" + Vogon.CurrentData.TolWandMinus + Vogon.Umbruch);
                                
                if (Vogon.TestLaenge())
                {
                    // Platz schaffen
                    builder.Append(Vogon.Umbruch + Vogon.Umbruch + Vogon.Umbruch);

                    // Vom Nutzer schon eingegebene, untere Werte (nur so zur Übersicht...)
                    // Länge
                    builder.Append(Vogon.Umbruch);
                    builder.Append(Languages.Get("keyLengthPerPieceOutput"));
                    builder.Append(Vogon.CurrentData.Laenge + " mm");

                    // Liefermenge
                    if (Vogon.TestLiefermengeKg() && Vogon.TestLiefermengeStk())
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        // In kg
                        builder.Append(Languages.Get("keyDeliveryQuantityOutput"));
                        builder.Append(Vogon.CurrentData.LiefermengeKg + " kg" + Vogon.Umbruch);

                        // In Stk.
                        builder.Append("                     ");
                        builder.Append(Vogon.CurrentData.LiefermengeStk);
                        builder.Append(Languages.Get("keyPcsOutput") + Vogon.Umbruch);
                    }

                    // Kosten
                    if (Calculate.kostenProStueck() != 0 && Calculate.kostenGesamt() != 0)
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        builder.Append(Languages.Get("keyMaterialCostsOutput"));
                        builder.Append(Vogon.CurrentData.Materialpreis + Languages.Get("keyEuroPerOutput") + " " + Vogon.CurrentData.MaterialpreisEinheit);

                        builder.Append(Vogon.Umbruch);

                        builder.Append(Languages.Get("keyHandlingChargesOutput"));
                        builder.Append(Vogon.CurrentData.Bearbeitungspreis + Languages.Get("keyEuroPerOutput") + " " + Vogon.CurrentData.BearbeitungspreisEinheit);
                    }
                }
            }

            return builder.ToString();
        }

        public static String CalculatedData()
        {
            StringBuilder builder = new StringBuilder();

            if (Vogon.TestAussen() && Vogon.TestInnen() && Vogon.TestWand() && Vogon.TestGewicht())
            {
                // Metergewichte
                builder.Append(Languages.Get("keyWeightperMeterOutput"));
                builder.Append(Calculate.kgProMeter().ToString("0.000000") + " kg/m" + Vogon.Umbruch);
                builder.Append("                     ");
                builder.Append(Calculate.meterProKG().ToString("0.000000") + " m/kg");

                if (Vogon.TestLaenge())
                {
                    // Platz schaffen
                    builder.Append(Vogon.Umbruch + Vogon.Umbruch + Vogon.Umbruch);

                    // Stückgewicht
                    builder.Append(Languages.Get("keyWeightPerPieceOutput"));
                    builder.Append(Calculate.stueckgewichtKgProStueck().ToString("0.000000"));
                    builder.Append(" kg" + Vogon.Umbruch);

                    // Liefermenge
                    if (Vogon.TestLiefermengeKg() && Vogon.TestLiefermengeStk())
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        // In Meter
                        builder.Append("                     ");
                        builder.Append(Calculate.liefermengeMeter().ToString("0.00") + " m");
                    }

                    // Kosten
                    if (Calculate.kostenProStueck() != 0 && Calculate.kostenGesamt() != 0)
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        builder.Append(Languages.Get("keyCostsPerPieceOutput"));
                        builder.Append(Calculate.kostenProStueck().ToString("0.00") + "€" + Vogon.Umbruch);

                        builder.Append(Languages.Get("keyCostsOutput"));
                        builder.Append(Calculate.kostenGesamt().ToString("0.00") + "€");
                    }
                }
            }

            return builder.ToString();
        }


        public static String Everything(int oldSaved, int index)
        {
            StringBuilder builder = new StringBuilder();

            if (Vogon.TestAussen(oldSaved, index) && Vogon.TestInnen(oldSaved, index) && Vogon.TestWand(oldSaved, index) && Vogon.TestGewicht(oldSaved, index))
            {
                // Vom Nutzer schon eingegebene, obere Werte (nur so zur Übersicht...)

                // Legierung
                builder.Append(Languages.Get("keyAlloyOutput"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Legierung + Vogon.Umbruch);

                // Dichte
                builder.Append(Languages.Get("keyDensityOutput"));
                builder.Append(Vogon.Data.Get(oldSaved, index).SpezGewicht + " g/cm³" + Vogon.Umbruch);

                // Außendurchmesser mit Toleranzen
                builder.Append(Languages.Get("keyOuterDiameterOutput"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Aussen + " mm   +" + Vogon.Data.Get(oldSaved, index).TolAussenPlus + "   -" + Vogon.Data.Get(oldSaved, index).TolAussenMinus + Vogon.Umbruch);

                // Innendurchmesser mit Toleranzen
                builder.Append(Languages.Get("keyInnerDiameterOutput"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Innen + " mm   +" + Vogon.Data.Get(oldSaved, index).TolInnenPlus + "   -" + Vogon.Data.Get(oldSaved, index).TolInnenMinus + Vogon.Umbruch);

                // Wanddicke mit Toleranzen
                builder.Append(Languages.Get("keyWallThicknessOutput"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Wand + " mm    +" + Vogon.Data.Get(oldSaved, index).TolWandPlus + "   -" + Vogon.Data.Get(oldSaved, index).TolWandMinus + Vogon.Umbruch);

                // Platz schaffen
                builder.Append(Vogon.Umbruch);


                // Metergewichte
                builder.Append(Languages.Get("keyWeightperMeterOutput"));
                builder.Append(Calculate.kgProMeter(oldSaved, index).ToString("0.000000") + " kg/m" + Vogon.Umbruch);
                builder.Append("                     ");
                builder.Append(Calculate.meterProKG(oldSaved, index).ToString("0.000000") + " m/kg");


                if (Vogon.TestLaenge(oldSaved, index))
                {
                    // Platz schaffen
                    builder.Append(Vogon.Umbruch + Vogon.Umbruch + Vogon.Umbruch);

                    // Stückgewicht
                    builder.Append(Languages.Get("keyWeightPerPieceOutput"));
                    builder.Append(Calculate.stueckgewichtKgProStueck(oldSaved, index).ToString("0.000000"));
                    builder.Append(" kg" + Vogon.Umbruch);


                    // Vom Nutzer schon eingegebene, untere Werte (nur so zur Übersicht...)
                    // Länge
                    builder.Append(Vogon.Umbruch);
                    builder.Append(Languages.Get("Length per Piece"));
                    builder.Append(Vogon.Data.Get(oldSaved, index).Laenge + " mm");

                    // Liefermenge
                    if (Vogon.TestLiefermengeKg(oldSaved, index) && Vogon.TestLiefermengeStk(oldSaved, index))
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        // In kg
                        builder.Append(Languages.Get("Delivery Quantity"));
                        builder.Append(Vogon.Data.Get(oldSaved, index).LiefermengeKg + " kg" + Vogon.Umbruch);

                        // In Stk.
                        builder.Append("                     ");
                        builder.Append(Vogon.Data.Get(oldSaved, index).LiefermengeStk);
                        builder.Append(Languages.Get("Pcs.") + Vogon.Umbruch);

                        // In Meter
                        builder.Append("                     ");
                        builder.Append(Calculate.liefermengeMeter(oldSaved, index).ToString("0.00") + " m");
                    }

                    // Kosten
                    if (Calculate.kostenProStueck(oldSaved, index) != 0 && Calculate.kostenGesamt(oldSaved, index) != 0)
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        builder.Append(Languages.Get("Material Costs") + "      ");
                        builder.Append(Vogon.Data.Get(oldSaved, index).Materialpreis + Languages.Get("Euro per") + " " + Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit);

                        builder.Append(Vogon.Umbruch);

                        builder.Append(Languages.Get("Handling Charges") + "  ");
                            builder.Append(Vogon.Data.Get(oldSaved, index).Bearbeitungspreis + Languages.Get("Euro per") + " " + Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit);

                        // Platz schaffen
                        builder.Append(Vogon.Umbruch);

                        builder.Append(Languages.Get("Costs per Piece") + "    ");
                        builder.Append(Calculate.kostenProStueck(oldSaved, index).ToString("0.00") + "€" + Vogon.Umbruch);

                        builder.Append(Languages.Get("Costs") + "  ");
                        builder.Append(Calculate.kostenGesamt(oldSaved, index).ToString("0.00") + "€");
                    }
                }
            }

            return builder.ToString();
        }

        public static String UserInput(int oldSaved, int index)
        {
            StringBuilder builder = new StringBuilder();

            if (Vogon.TestAussen(oldSaved, index) && Vogon.TestInnen(oldSaved, index) && Vogon.TestWand(oldSaved, index) && Vogon.TestGewicht(oldSaved, index))
            {
                // Vom Nutzer schon eingegebene, obere Werte (nur so zur Übersicht...)

                // Legierung
                builder.Append(Languages.Get("Alloy"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Legierung + Vogon.Umbruch);

                // Dichte
                builder.Append(Languages.Get("Density"));
                builder.Append(Vogon.Data.Get(oldSaved, index).SpezGewicht + " g/cm³" + Vogon.Umbruch);

                // Außendurchmesser mit Toleranzen
                builder.Append(Languages.Get("Outer Diameter"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Aussen + " mm   +" + Vogon.Data.Get(oldSaved, index).TolAussenPlus + "   -" + Vogon.Data.Get(oldSaved, index).TolAussenMinus + Vogon.Umbruch);

                // Innendurchmesser mit Toleranzen
                builder.Append(Languages.Get("Inner Diameter"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Innen + " mm   +" + Vogon.Data.Get(oldSaved, index).TolInnenPlus + "   -" + Vogon.Data.Get(oldSaved, index).TolInnenMinus + Vogon.Umbruch);

                // Wanddicke mit Toleranzen
                builder.Append(Languages.Get("Wall Thickness"));
                builder.Append(Vogon.Data.Get(oldSaved, index).Wand + " mm    +" + Vogon.Data.Get(oldSaved, index).TolWandPlus + "   -" + Vogon.Data.Get(oldSaved, index).TolWandMinus + Vogon.Umbruch);

                if (Vogon.TestLaenge(oldSaved, index))
                {
                    // Platz schaffen
                    builder.Append(Vogon.Umbruch + Vogon.Umbruch + Vogon.Umbruch);

                    // Vom Nutzer schon eingegebene, untere Werte (nur so zur Übersicht...)
                    // Länge
                    builder.Append(Vogon.Umbruch);
                    builder.Append(Languages.Get("Length per Piece"));
                    builder.Append(Vogon.Data.Get(oldSaved, index).Laenge + " mm");

                    // Liefermenge
                    if (Vogon.TestLiefermengeKg(oldSaved, index) && Vogon.TestLiefermengeStk(oldSaved, index))
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        // In kg
                        builder.Append(Languages.Get("Delivery Quantity"));
                        builder.Append(Vogon.Data.Get(oldSaved, index).LiefermengeKg + " kg" + Vogon.Umbruch);

                        // In Stk.
                        builder.Append("                     ");
                        builder.Append(Vogon.Data.Get(oldSaved, index).LiefermengeStk);
                        builder.Append(Languages.Get("Pcs.") + Vogon.Umbruch);
                    }

                    // Kosten
                    if (Calculate.kostenProStueck(oldSaved, index) != 0 && Calculate.kostenGesamt(oldSaved, index) != 0)
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        builder.Append(Languages.Get("Material Costs") + "      ");
                        builder.Append(Vogon.Data.Get(oldSaved, index).Materialpreis + Languages.Get("Euro per") + " " + Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit);

                        builder.Append(Vogon.Umbruch);

                        builder.Append(Languages.Get("Handling Charges") + "  ");
                        builder.Append(Vogon.Data.Get(oldSaved, index).Bearbeitungspreis + Languages.Get("Euro per") + " " + Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit);
                    }
                }
            }

            return builder.ToString();
        }

        public static String CalculatedData(int oldSaved, int index)
        {
            StringBuilder builder = new StringBuilder();

            if (Vogon.TestAussen(oldSaved, index) && Vogon.TestInnen(oldSaved, index) && Vogon.TestWand(oldSaved, index) && Vogon.TestGewicht(oldSaved, index))
            {
                // Metergewichte
                builder.Append(Languages.Get("Weight per Meter"));
                builder.Append(Calculate.kgProMeter(oldSaved, index).ToString("0.000000") + " kg/m" + Vogon.Umbruch);
                builder.Append(Calculate.meterProKG(oldSaved, index).ToString("0.000000") + " m/kg");

                if (Vogon.TestLaenge(oldSaved, index))
                {
                    // Platz schaffen
                    builder.Append(Vogon.Umbruch + Vogon.Umbruch + Vogon.Umbruch);

                    // Stückgewicht
                    builder.Append(Languages.Get("Weight per Piece"));
                    builder.Append(Calculate.stueckgewichtKgProStueck(oldSaved, index).ToString("0.000000"));
                    builder.Append(" kg/Stk." + Vogon.Umbruch);

                    // Liefermenge
                    if (Vogon.TestLiefermengeKg(oldSaved, index) && Vogon.TestLiefermengeStk(oldSaved, index))
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        // In Meter
                        builder.Append("                     ");
                        builder.Append(Calculate.liefermengeMeter(oldSaved, index).ToString("0.00") + " m");
                    }

                    // Kosten
                    if (Calculate.kostenProStueck(oldSaved, index) != 0 && Calculate.kostenGesamt(oldSaved, index) != 0)
                    {
                        // Platz schaffen
                        builder.Append(Vogon.Umbruch + Vogon.Umbruch);

                        builder.Append(Languages.Get("Costs per Piece") + "    ");
                        builder.Append(Calculate.kostenProStueck(oldSaved, index).ToString("0.00") + "€" + Vogon.Umbruch);

                        builder.Append(Languages.Get("Costs") + "  ");
                        builder.Append(Calculate.kostenGesamt(oldSaved, index).ToString("0.00") + "€");
                    }
                }
            }

            return builder.ToString();
        }
    }
}
