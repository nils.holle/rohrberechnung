﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rohrberechnung.Calc
{
    public static class AlloyList
    {
        public static List<Alloy> List = new List<Alloy>();
        
        public static void GetList()
        {
            List.Clear();
            string reader = "Bla;2,7";  // Properties.Settings.Default.AlloyList;
            string[] readerSplitted = reader.Split(new string[] {";", "\n"}, StringSplitOptions.None);

            if (readerSplitted.Length > 1)
            {
                for (int i = 0; i < readerSplitted.Length; i += 2)
                {
                    Alloy alloy = new Alloy();
                    alloy.AlloyName = readerSplitted[i];
                    alloy.Density = Calculate.StringToDouble(readerSplitted[i + 1]);
                    if (alloy.AlloyName != "" && alloy.Density != 0) List.Add(alloy);
                }
            }
        }

        public static double GetDensity(string alloy)
        {
            if (alloy != "") return (from temp in List where temp.AlloyName == alloy select temp).FirstOrDefault().Density;
            else return 0;
        }

        public static void SaveAlloys()
        {
            string alloyString = "";

            foreach (Alloy alloyTemp in List)
            {
                alloyString +=  Vogon.Umbruch + alloyTemp.AlloyName + ";" + alloyTemp.Density.ToString();
            }

            // Properties.Settings.Default.AlloyList = alloyString;
            // Properties.Settings.Default.Save();
        }
    }
}
