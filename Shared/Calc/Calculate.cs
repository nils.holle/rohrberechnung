﻿using System;

namespace Rohrberechnung.Calc
{
    public static class Calculate
    {
        public static double kgProMeter()
        {
            return ((aussenDurchmesser() - wandStaerke()) * wandStaerke() * Vogon.CurrentData.SpezGewicht * Math.PI) / 1000;
        }

        public static double kgProMeter(int oldSaved, int index)
        {
            return ((aussenDurchmesser(oldSaved, index) - wandStaerke(oldSaved, index)) * wandStaerke(oldSaved, index) * Vogon.Data.Get(oldSaved, index).SpezGewicht * Math.PI) / 1000;
        }


        public static double meterProKG()
        {
            double temp = kgProMeter();
            if (temp != 0) return 1 / temp;
            else return 0;
        }

        public static double meterProKG(int oldSaved, int index)
        {
            double temp = kgProMeter(oldSaved, index);
            if (temp != 0) return 1 / temp;
            else return 0;
        }


        public static double stueckgewichtKgProStueck()
        {
            return kgProMeter() * Vogon.CurrentData.Laenge / 1000;
        }

        public static double stueckgewichtKgProStueck(int oldSaved, int index)
        {
            return kgProMeter(oldSaved, index) * Vogon.Data.Get(oldSaved, index).Laenge / 1000;
        }


        public static double liefermengeMeter()
        {
            return Vogon.CurrentData.LiefermengeStk * (Vogon.CurrentData.Laenge / 1000);
        }

        public static double liefermengeMeter(int oldSaved, int index)
        {
            return Vogon.Data.Get(oldSaved, index).LiefermengeStk * (Vogon.Data.Get(oldSaved, index).Laenge / 1000);
        }



        public static double aussenDurchmesser()
        {
            return Vogon.CurrentData.Aussen + (Vogon.CurrentData.TolAussenPlus - Vogon.CurrentData.TolAussenMinus) / 2;
        }

        public static double aussenDurchmesser(int oldSaved, int index)
        {
            return Vogon.Data.Get(oldSaved, index).Aussen + (Vogon.Data.Get(oldSaved, index).TolAussenPlus - Vogon.Data.Get(oldSaved, index).TolAussenMinus) / 2;
        }


        public static double innenDurchmesser()
        {
            return Vogon.CurrentData.Innen + (Vogon.CurrentData.TolInnenPlus - Vogon.CurrentData.TolInnenMinus) / 2;
        }

        public static double innenDurchmesser(int oldSaved, int index)
        {
            return Vogon.Data.Get(oldSaved, index).Innen + (Vogon.Data.Get(oldSaved, index).TolInnenPlus - Vogon.Data.Get(oldSaved, index).TolInnenMinus) / 2;
        }


        public static double wandStaerke()
        {
            return Vogon.CurrentData.Wand + (Vogon.CurrentData.TolWandPlus - Vogon.CurrentData.TolWandMinus) / 2;
        }

        public static double wandStaerke(int oldSaved, int index)
        {
            return Vogon.Data.Get(oldSaved, index).Wand + (Vogon.Data.Get(oldSaved, index).TolWandPlus - Vogon.Data.Get(oldSaved, index).TolWandMinus) / 2;
        }


        public static double kostenProStueck()
        {
            double materialkosten = 0;
            double bearbeitungskosten = 0;

            // Materialkosten:

            if (Vogon.CurrentData.MaterialpreisEinheit == "m") materialkosten = Vogon.CurrentData.Materialpreis * (Vogon.CurrentData.Laenge / 1000);
            else if (Vogon.CurrentData.MaterialpreisEinheit == "100m") materialkosten = Vogon.CurrentData.Materialpreis * (Vogon.CurrentData.Laenge / 100000);
            else if (Vogon.CurrentData.MaterialpreisEinheit == "kg") materialkosten = Vogon.CurrentData.Materialpreis * stueckgewichtKgProStueck();
            else if (Vogon.CurrentData.MaterialpreisEinheit == "100kg") materialkosten = Vogon.CurrentData.Materialpreis * (stueckgewichtKgProStueck() / 100);
            else if (Vogon.CurrentData.MaterialpreisEinheit == "to") materialkosten = Vogon.CurrentData.Materialpreis * (stueckgewichtKgProStueck() / 1000);


            // Bearbeitungskosten:

            if (Vogon.CurrentData.BearbeitungspreisEinheit == "m") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (Vogon.CurrentData.Laenge / 1000);
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "100m") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (Vogon.CurrentData.Laenge / 100000);
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "kg") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * stueckgewichtKgProStueck();
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "100kg") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (stueckgewichtKgProStueck() / 100);
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "to") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (stueckgewichtKgProStueck() / 1000);

            return materialkosten + bearbeitungskosten;
        }

        public static double kostenProStueck(int oldSaved, int index)
        {
            double materialkosten = 0;
            double bearbeitungskosten = 0;

             // Materialkosten:

                if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "m") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (Vogon.Data.Get(oldSaved, index).Laenge) / 1000;
                    else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "100m") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (Vogon.Data.Get(oldSaved, index).Laenge) / 100000;
                    else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "kg") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * stueckgewichtKgProStueck(oldSaved, index);
                    else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "100kg") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (stueckgewichtKgProStueck(oldSaved, index) / 100);
                    else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "to") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (stueckgewichtKgProStueck(oldSaved, index) / 1000);
                

                // Bearbeitungskosten:

                if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "m") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (Vogon.Data.Get(oldSaved, index).Laenge) / 1000;
                    else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "100m") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (Vogon.Data.Get(oldSaved, index).Laenge) / 100000;
                    else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "kg") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * stueckgewichtKgProStueck(oldSaved, index);
                    else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "100kg") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (stueckgewichtKgProStueck(oldSaved, index) / 100);
                    else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "to") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (stueckgewichtKgProStueck(oldSaved, index) / 1000);
                
            return materialkosten + bearbeitungskosten;
        }

        public static double kostenGesamt()
        {
            double materialkosten = 0;
            double bearbeitungskosten = 0;

            // Materialkosten:

            if (Vogon.CurrentData.MaterialpreisEinheit == "m") materialkosten = Vogon.CurrentData.Materialpreis * liefermengeMeter();
            else if (Vogon.CurrentData.MaterialpreisEinheit == "100m") materialkosten = Vogon.CurrentData.Materialpreis * (liefermengeMeter() / 100);
            else if (Vogon.CurrentData.MaterialpreisEinheit == "kg") materialkosten = Vogon.CurrentData.Materialpreis * Vogon.CurrentData.LiefermengeKg;
            else if (Vogon.CurrentData.MaterialpreisEinheit == "100kg") materialkosten = Vogon.CurrentData.Materialpreis * (Vogon.CurrentData.LiefermengeKg / 100);
            else if (Vogon.CurrentData.MaterialpreisEinheit == "to") materialkosten = Vogon.CurrentData.Materialpreis * (Vogon.CurrentData.LiefermengeKg / 1000);

            // Bearbeitungskosten:

            if (Vogon.CurrentData.BearbeitungspreisEinheit == "m") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * liefermengeMeter();
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "100m") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (liefermengeMeter() / 100);
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "kg") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * Vogon.CurrentData.LiefermengeKg;
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "100kg") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (Vogon.CurrentData.LiefermengeKg / 100);
            else if (Vogon.CurrentData.BearbeitungspreisEinheit == "to") bearbeitungskosten = Vogon.CurrentData.Bearbeitungspreis * (Vogon.CurrentData.LiefermengeKg / 1000);

            return materialkosten + bearbeitungskosten;
        }

        public static double kostenGesamt(int oldSaved, int index)
        {
            double materialkosten = 0;
            double bearbeitungskosten = 0;

            // Materialkosten:

            if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "m") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * liefermengeMeter(oldSaved, index);
            else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "100m") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (liefermengeMeter(oldSaved, index) / 100);
            else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "kg") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * Vogon.Data.Get(oldSaved, index).LiefermengeKg;
            else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "100kg") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (Vogon.Data.Get(oldSaved, index).LiefermengeKg / 100);
            else if (Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit == "to") materialkosten = Vogon.Data.Get(oldSaved, index).Materialpreis * (Vogon.Data.Get(oldSaved, index).LiefermengeKg / 1000);

            // Bearbeitungskosten:

            if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "m") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * liefermengeMeter(oldSaved, index);
            else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "m") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (liefermengeMeter(oldSaved, index) / 100);
            else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "kg") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * Vogon.Data.Get(oldSaved, index).LiefermengeKg;
            else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "100kg") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (Vogon.Data.Get(oldSaved, index).LiefermengeKg) / 100;
            else if (Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit == "to") bearbeitungskosten = Vogon.Data.Get(oldSaved, index).Bearbeitungspreis * (Vogon.Data.Get(oldSaved, index).LiefermengeKg) / 1000;

            return materialkosten + bearbeitungskosten;
        }

        private static double missingOuterDiameter()
        {
            return Vogon.CurrentData.Innen + 2 * Vogon.CurrentData.Wand;
        }

        private static double missingInnerDiameter()
        {
            return Vogon.CurrentData.Aussen - 2 * Vogon.CurrentData.Wand;
        }

        private static double missingWallThickness()
        {
            return (Vogon.CurrentData.Aussen - Vogon.CurrentData.Innen) / 2;
        }

        private static double missingDeliveryQuantityKg()
        {
            return stueckgewichtKgProStueck() * Vogon.CurrentData.LiefermengeStk;
        }

        private static double missingDeliveryQuantityPcs()
        {
            if (stueckgewichtKgProStueck() != 0) return Math.Truncate(Vogon.CurrentData.LiefermengeKg / stueckgewichtKgProStueck());
            else return 0;
        }

        public static DataSet CalculateMissing(DataSet input, int changed, int hold)
        {
            // hold: 0 Standard, 1 Aussen, 2 Innen, 3 Wand
            Vogon.CurrentData = input;

            switch (changed)
            {
                case 1:
                    Vogon.CurrentData.SpezGewicht = AlloyList.GetDensity(Vogon.CurrentData.Legierung);
                    Vogon.CurrentData.LiefermengeStk = missingDeliveryQuantityPcs();
                    break;
                case 2: break; // Aktion wenn Spez. Gewicht geändert
                case 3:
                    if (hold != 3 && Vogon.TestInnen()) Vogon.CurrentData.Wand = missingWallThickness();
                    else if (hold != 2 && Vogon.TestWand()) Vogon.CurrentData.Innen = missingInnerDiameter();
                    break;
                case 6:
                    if (hold != 3 && Vogon.TestAussen()) Vogon.CurrentData.Wand = missingWallThickness();
                    else if (hold != 1 && Vogon.TestWand()) Vogon.CurrentData.Aussen = missingOuterDiameter();
                    break;
                case 9:
                    if (hold != 2 && Vogon.TestWand()) Vogon.CurrentData.Innen = missingInnerDiameter();
                    else if (hold != 1 && Vogon.TestWand()) Vogon.CurrentData.Aussen = missingOuterDiameter();
                    break;
                case 12:
                    Vogon.CurrentData.LiefermengeStk = missingDeliveryQuantityPcs();
                    break;
                case 13:
                    Vogon.CurrentData.LiefermengeStk = missingDeliveryQuantityPcs();
                    break;
                case 14:
                    Vogon.CurrentData.LiefermengeKg = missingDeliveryQuantityKg();
                    break;
            }

            return Vogon.CurrentData;
        }

        public static String[] DataSetToString(DataSet input)
        {
            string[] output = new string[19];

            output[0] = input.Zeit;
            output[1] = input.Legierung;
            output[2] = DoubleToString(input.SpezGewicht);
            output[3] = DoubleToString(input.Aussen);
            output[4] = DoubleToString(input.TolAussenPlus);
            output[5] = DoubleToString(input.TolAussenMinus);
            output[6] = DoubleToString(input.Innen);
            output[7] = DoubleToString(input.TolInnenPlus);
            output[8] = DoubleToString(input.TolInnenMinus);
            output[9] = DoubleToString(input.Wand);
            output[10] = DoubleToString(input.TolWandPlus);
            output[11] = DoubleToString(input.TolWandMinus);
            output[12] = DoubleToString(input.Laenge);
            output[13] = DoubleToString(input.LiefermengeKg);
            output[14] = DoubleToString(input.LiefermengeStk);
            output[15] = DoubleToString(input.Materialpreis);
            output[16] = input.MaterialpreisEinheit;
            output[17] = DoubleToString(input.Bearbeitungspreis);
            output[18] = input.BearbeitungspreisEinheit;

            return output;
        }

        public static DataSet StringToDataSet(string[] input)
        {
            DataSet output = new DataSet();

            output.Zeit = input[0];
            output.Legierung = input[1];
            output.SpezGewicht = StringToDouble(input[2]);
            output.Aussen = StringToDouble(input[3]);
            output.TolAussenPlus = StringToDouble(input[4]);
            output.TolAussenMinus = StringToDouble(input[5]);
            output.Innen = StringToDouble(input[6]);
            output.TolInnenPlus = StringToDouble(input[7]);
            output.TolInnenMinus = StringToDouble(input[8]);
            output.Wand = StringToDouble(input[9]);
            output.TolWandPlus = StringToDouble(input[10]);
            output.TolWandMinus = StringToDouble(input[11]);
            output.Laenge = StringToDouble(input[12]);
            output.LiefermengeKg = StringToDouble(input[13]);
            output.LiefermengeStk = StringToDouble(input[14]);
            output.Materialpreis = StringToDouble(input[15]);
            output.MaterialpreisEinheit = input[16];
            output.Bearbeitungspreis = StringToDouble(input[17]);
            output.BearbeitungspreisEinheit = input[18];

            return output;
        }

        public static Double StringToDouble(string input)
        {
            try
            {
                input = input.Replace(",", ".");
                return Double.Parse(input, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        public static String DoubleToString(double input)
        {
            if (input == 0) return "";
            else return input.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public static String DoubleToString(double input, string format)
        {
            if (input == 0) return "";
            else return input.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
        }
    }
}
