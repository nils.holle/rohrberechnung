﻿using System;

namespace Rohrberechnung.Calc
{
    public class DataSet
    {
        private string mZeit = "";
        private string mLegierung = "";
        private double mSpezGewicht = 0;
        private double mAussen = 0;
        private double mTolAussenPlus = 0;
        private double mTolAussenMinus = 0;
        private double mInnen = 0;
        private double mTolInnenPlus = 0;
        private double mTolInnenMinus = 0;
        private double mWand = 0;
        private double mTolWandPlus = 0;
        private double mTolWandMinus = 0;
        private double mLaenge = 0;
        private double mLiefermengeKg = 0;
        private double mLiefermengeStk = 0;
        private double mMaterialpreis = 0;
        private string mMaterialpreisEinheit = "";
        private double mBearbeitungspreis = 0;
        private string mBearbeitungspreisEinheit = "";

        public void Delete()
        {
            mZeit = "";
            mLegierung = "";
            mSpezGewicht = 0;
            mAussen = 0;
            mTolAussenPlus = 0;
            mTolAussenMinus = 0;
            mInnen = 0;
            mTolInnenPlus = 0;
            mTolInnenMinus = 0;
            mWand = 0;
            mTolWandPlus = 0;
            mTolWandMinus = 0;
            mLaenge = 0;
            mLiefermengeKg = 0;
            mLiefermengeStk = 0;
            mMaterialpreis = 0;
            mMaterialpreisEinheit = "";
            mBearbeitungspreis = 0;
            mBearbeitungspreisEinheit = "";
        }

        public String Zeit
        {
            get { return mZeit; }
            set { mZeit = value; }
        }

        public String Legierung
        {
            get { return mLegierung; }
            set { mLegierung = value; }
        }

        public Double SpezGewicht
        {
            get { return mSpezGewicht; }
            set { mSpezGewicht = value; }
        }

        public Double Aussen
        {
            get { return mAussen; }
            set { mAussen = value; }
        }

        public Double TolAussenPlus
        {
            get { return mTolAussenPlus; }
            set { mTolAussenPlus = value; }
        }

        public Double TolAussenMinus
        {
            get { return mTolAussenMinus; }
            set { mTolAussenMinus = value; }
        }

        public Double Innen
        {
            get { return mInnen; }
            set { mInnen = value; }
        }

        public Double TolInnenPlus
        {
            get { return mTolInnenPlus; }
            set { mTolInnenPlus = value; }
        }

        public Double TolInnenMinus
        {
            get { return mTolInnenMinus; }
            set { mTolInnenMinus = value; }
        }

        public Double Wand
        {
            get { return mWand; }
            set { mWand = value; }
        }

        public Double TolWandPlus
        {
            get { return mTolWandPlus; }
            set { mTolWandPlus = value; }
        }

        public Double TolWandMinus
        {
            get { return mTolWandMinus; }
            set { mTolWandMinus = value; }
        }

        public Double Laenge
        {
            get { return mLaenge; }
            set { mLaenge = value; }
        }

        public Double LiefermengeKg
        {
            get { return mLiefermengeKg; }
            set { mLiefermengeKg = value; }
        }

        public Double LiefermengeStk
        {
            get { return mLiefermengeStk; }
            set { mLiefermengeStk = value; }
        }

        public Double Materialpreis
        {
            get { return mMaterialpreis; }
            set { mMaterialpreis = value; }
        }

        public String MaterialpreisEinheit
        {
            get { return mMaterialpreisEinheit; }
            set { mMaterialpreisEinheit = value; }
        }

        public Double Bearbeitungspreis
        {
            get { return mBearbeitungspreis; }
            set { mBearbeitungspreis = value; }
        }

        public String BearbeitungspreisEinheit
        {
            get { return mBearbeitungspreisEinheit; }
            set { mBearbeitungspreisEinheit = value; }
        }
    }
}
