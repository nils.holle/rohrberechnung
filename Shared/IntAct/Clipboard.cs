﻿using Microsoft.JSInterop;

namespace Rohrberechnung.IntAct
{
    public class Clipboard
    {
        private readonly IJSRuntime js;

        public Clipboard(IJSRuntime js)
        {
            this.js = js;
        }

        private static void copyToClipboard(string text)
        {
            if (text != "")
            {
                // System.Windows.Clipboard.Clear();
                // System.Windows.Clipboard.SetText(text);

                Calc.Vogon.Info(Calc.Languages.Get("keyCopySuccessfull"));
            }
            else Calc.Vogon.Error(Calc.Languages.Get("keyErrorNoData"));
        }


        public static void CopyEverything()
        {
            copyToClipboard(Calc.DataAsText.Everything());
        }

        public static void CopyUserInput()
        {
            copyToClipboard(Calc.DataAsText.UserInput());
        }

        public static void CopyCalculatedData()
        {
            copyToClipboard(Calc.DataAsText.CalculatedData());
        }


        public static void CopyEverything(int oldSaved, int index)
        {
            copyToClipboard(Calc.DataAsText.Everything(oldSaved, index));
        }

        public static void CopyUserInput(int oldSaved, int index)
        {
            copyToClipboard(Calc.DataAsText.UserInput(oldSaved, index));
        }

        public static void CopyCalculatedData(int oldSaved, int index)
        {
            copyToClipboard(Calc.DataAsText.CalculatedData(oldSaved, index));
        }
    }
}
