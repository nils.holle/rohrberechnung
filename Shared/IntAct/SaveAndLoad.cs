﻿using System;
using System.IO;

namespace Rohrberechnung.IntAct
{
    public static class SaveAndLoad
    {
        private static string Path = "";

        public static void SaveData()
        {
            SaveDataAs(Path);
        }

        public static void SaveDataAs(string path = "")
        {
            if (path == "")
            {
                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // if (Properties.Settings.Default.Language == "de") save.Filter = "Rohrberechnungs-Dateien (*.tub)|*.tub";
                // else save.Filter = "Rohrberechnung - data (*.tub)|*.tub";
                // if (Properties.Settings.Default.Language == "de") save.Title = "Datei speichern unter";
                // else save.Title = "Save file as...";

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     path = save.FileName;
                //     Path = System.IO.Path.GetFileName(path);
                // }
            }

            if (path != "")
            {
                FileStream stream = new FileStream(path, FileMode.Create);
                StreamWriter writer = new StreamWriter(stream);

                // Alte Daten

                writer.WriteLine("Rohrberechnung");
                writer.WriteLine("!");

                for (int i = 0; i < Calc.Vogon.Data.Count(0); i++)
                {
                    writer.Write(Calc.Vogon.Data.Get(0, i).Zeit + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Legierung + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).SpezGewicht + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Aussen + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).TolAussenPlus + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).TolAussenMinus + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Innen + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).TolInnenPlus + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).TolInnenMinus + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Wand + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).TolWandPlus + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).TolWandMinus + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Laenge + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).LiefermengeKg + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).LiefermengeStk + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Materialpreis + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).MaterialpreisEinheit + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Bearbeitungspreis + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).BearbeitungspreisEinheit + ";");
                    writer.WriteLine();
                }

                writer.WriteLine("!");

                // Gespeicherte Daten

                for (int i = 0; i < Calc.Vogon.Data.Count(1); i++)
                {
                    writer.Write(Calc.Vogon.Data.Get(1, i).Zeit + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).Legierung + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).SpezGewicht + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).Aussen + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).TolAussenPlus + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).TolAussenMinus + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).Innen + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).TolInnenPlus + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).TolInnenMinus + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).Wand + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).TolWandPlus + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).TolWandMinus + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).Laenge + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).LiefermengeKg + ";");
                    writer.Write(Calc.Vogon.Data.Get(1, i).LiefermengeStk + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Materialpreis + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).MaterialpreisEinheit + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).Bearbeitungspreis + ";");
                    writer.Write(Calc.Vogon.Data.Get(0, i).BearbeitungspreisEinheit + ";");
                    writer.WriteLine();
                }

                writer.Write("!");

                writer.Flush();
                writer.Close();
                writer.Dispose();
                stream.Close();
                stream.Dispose();
                Calc.Vogon.Info(Calc.Languages.Get("keySaveSuccessfull"));
            }
        }

        public static void LoadData()
        {
            // OpenFileDialog open = new OpenFileDialog();

            // if (Path != "") open.InitialDirectory = Path;
            // else open.InitialDirectory = System.Environment.SpecialFolder.MyDocuments.ToString();
            // if (Properties.Settings.Default.Language == "de") open.Filter = "Rohrberechnungs-Dateien (*.tub)|*.tub";
            // else open.Filter = "Rohrberechnung - data (*.tub)|*.tub";
            // if (Properties.Settings.Default.Language == "de") open.Title = "Datei öffnen";
            // else open.Title = "Open data";

            // if (open.ShowDialog() == DialogResult.OK)
            // {
            //     loadDataReal(open.FileName);
            //     Path = open.FileName;

            //     open.Dispose();
            //     Calc.Vogon.Info(Calc.Languages.Get("keyFileOpened"));
            // }
        }

        public static void LoadData(string Path)
        {
            if (Path != "") loadDataReal(Path);
        }

        private static void loadDataReal(string Path)
        {
            StreamReader reader = new StreamReader(Path);

            Calc.Vogon.Data.RemoveAll(0);
            Calc.Vogon.Data.RemoveAll(1);

            // Alte Daten

            if (reader.ReadLine() == "Rohrberechnung")
            {
                if (reader.ReadLine() == "!")
                {
                    int i = 0;
                    while (reader.Peek() != '!')
                    {
                        String[] temp = reader.ReadLine().Split(';');

                        Calc.DataSet row = new Calc.DataSet();

                        row.Zeit = temp[0];
                        row.Legierung = temp[1];
                        row.SpezGewicht = Convert.ToDouble(temp[2]);
                        row.Aussen = Convert.ToDouble(temp[3]);
                        row.TolAussenPlus = Convert.ToDouble(temp[4]);
                        row.TolAussenMinus = Convert.ToDouble(temp[5]);
                        row.Innen = Convert.ToDouble(temp[6]);
                        row.TolInnenPlus = Convert.ToDouble(temp[7]);
                        row.TolInnenMinus = Convert.ToDouble(temp[8]);
                        row.Wand = Convert.ToDouble(temp[9]);
                        row.TolWandPlus = Convert.ToDouble(temp[10]);
                        row.TolWandMinus = Convert.ToDouble(temp[11]);
                        row.Laenge = Convert.ToDouble(temp[12]);
                        row.LiefermengeKg = Convert.ToDouble(temp[13]);
                        row.LiefermengeStk = Convert.ToDouble(temp[14]);
                        row.Materialpreis = Convert.ToDouble(temp[15]);
                        row.MaterialpreisEinheit = temp[16];
                        row.Bearbeitungspreis = Convert.ToDouble(temp[17]);
                        row.BearbeitungspreisEinheit = temp[18];

                        Calc.Vogon.Data.Add(0, row);
                        i++;
                    }

                    reader.ReadLine();

                    // Gespeicherte Daten

                    i = 0;
                    while (reader.Peek() != '!')
                    {
                        String[] temp = reader.ReadLine().Split(';');

                        Calc.DataSet row = new Calc.DataSet();

                        row.Zeit = temp[0];
                        row.Legierung = temp[1];
                        row.SpezGewicht = Convert.ToDouble(temp[2]);
                        row.Aussen = Convert.ToDouble(temp[3]);
                        row.TolAussenPlus = Convert.ToDouble(temp[4]);
                        row.TolAussenMinus = Convert.ToDouble(temp[5]);
                        row.Innen = Convert.ToDouble(temp[6]);
                        row.TolInnenPlus = Convert.ToDouble(temp[7]);
                        row.TolInnenMinus = Convert.ToDouble(temp[8]);
                        row.Wand = Convert.ToDouble(temp[9]);
                        row.TolWandPlus = Convert.ToDouble(temp[10]);
                        row.TolWandMinus = Convert.ToDouble(temp[11]);
                        row.Laenge = Convert.ToDouble(temp[12]);
                        row.LiefermengeKg = Convert.ToDouble(temp[13]);
                        row.LiefermengeStk = Convert.ToDouble(temp[14]);
                        row.Materialpreis = Convert.ToDouble(temp[15]);
                        row.MaterialpreisEinheit = temp[16];
                        row.Bearbeitungspreis = Convert.ToDouble(temp[17]);
                        row.BearbeitungspreisEinheit = temp[18];

                        Calc.Vogon.Data.Add(1, row);
                        i++;
                    }

                    Calc.Vogon.CurrentData = Calc.Vogon.Data.Get(0, Calc.Vogon.Data.Count(0) - 1);
                }
                else loadError();
            }
            else loadError();

            reader.Close();
            reader.Dispose();
        }

        private static void loadError()
        {
            Calc.Vogon.Error(Calc.Languages.Get("keyErrorLoad"));
        }
    }
}
