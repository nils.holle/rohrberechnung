﻿namespace Rohrberechnung.IntAct
{
    public static class Print
    {
        private static string Text;

        private static void printText(string text)
        {
            if (text != "")
            {
                Text = text;
                // PrintDialog printDia = new PrintDialog();

                // if (printDia.ShowDialog() == DialogResult.OK)
                // {
                //     PrintDocument printDoc = new PrintDocument();
                //     printDoc.DocumentName = "Rohrberechnung";
                //     printDoc.PrintPage += new PrintPageEventHandler(printPage);
                //     printDoc.PrinterSettings = printDia.PrinterSettings;
                //     printDoc.Print();
                //     Calc.Vogon.Info(Calc.Languages.Get("keyPrintSuccessfull"));
                // }
            }
            else Calc.Vogon.Error(Calc.Languages.Get("keyErrorNoData"));
        }

        // private static void printPage(object sender, PrintPageEventArgs e)
        // {
        //     int y;
        //     // Print receipt
        //     Font myFont = new Font("Lucida Console", 14, FontStyle.Regular);
        //     y = e.MarginBounds.Y;
        //     e.Graphics.DrawString(Text, myFont, Brushes.Black, e.MarginBounds.X, y);
        // }


        public static void PrintEverything()
        {
            printText(Calc.DataAsText.Everything());
        }

        public static void PrintUserInput()
        {
            printText(Calc.DataAsText.UserInput());
        }

        public static void PrintCalculatedData()
        {
            printText(Calc.DataAsText.CalculatedData());
        }


        public static void PrintEverything(int oldSaved, int index)
        {
            printText(Calc.DataAsText.Everything(oldSaved, index));
        }

        public static void PrintUserInput(int oldSaved, int index)
        {
            printText(Calc.DataAsText.UserInput(oldSaved, index));
        }

        public static void PrintCalculatedData(int oldSaved, int index)
        {
            printText(Calc.DataAsText.CalculatedData(oldSaved, index));
        }
    }
}
