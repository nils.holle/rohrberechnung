﻿using System.Diagnostics;

namespace Rohrberechnung.IntAct
{
    public static class Mail
    {
        private static void sendMail(string text)
        {
            if (text != "")
            {
                Process.Start("mailto:?body=" + text.Replace("\r\n", "%0A"));
                Calc.Vogon.Info(Calc.Languages.Get("keyMailSuccessfull"));
            }
            else Calc.Vogon.Error(Calc.Languages.Get("keyErrorNoData"));
        }

        public static void MailEverything()
        {
            sendMail(Calc.DataAsText.Everything());
        }

        public static void MailUserInput()
        {
            sendMail(Calc.DataAsText.UserInput());
        }

        public static void MailCalculatedData()
        {
            sendMail(Calc.DataAsText.CalculatedData());
        }


        public static void MailEverything(int oldSaved, int index)
        {
            sendMail(Calc.DataAsText.Everything(oldSaved, index));
        }

        public static void MailUserInput(int oldSaved, int index)
        {
            sendMail(Calc.DataAsText.UserInput(oldSaved, index));
        }

        public static void MailCalculatedData(int oldSaved, int index)
        {
            sendMail(Calc.DataAsText.CalculatedData(oldSaved, index));
        }
    }
}
