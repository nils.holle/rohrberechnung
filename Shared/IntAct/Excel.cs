﻿using System;
using System.Text;
using System.Xml;
using System.Data;

namespace Rohrberechnung.IntAct
{
    public static class Excel
    {
        private static string excelCurrentFilename;
        
        private static void exportDataTableToWorksheet(DataTable dataSource, string fileName)
        {
            // XML-Schreiber erzeugen
            XmlTextWriter writer = new XmlTextWriter(fileName, Encoding.UTF8);

            // Ausgabedatei für bessere Lesbarkeit formatieren (einrücken etc.)
            writer.Formatting = Formatting.Indented;

            // <?xml version="1.0"?>
            writer.WriteStartDocument();

            // <?mso-application progid="Excel.Sheet"?>
            writer.WriteProcessingInstruction("mso-application", "progid=\"Excel.Sheet\"");

            // <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet >"
            writer.WriteStartElement("Workbook", "urn:schemas-microsoft-com:office:spreadsheet");

            // Definition der Namensräume schreiben 
            writer.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
            writer.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
            writer.WriteAttributeString("xmlns", "ss", null, "urn:schemas-microsoft-com:office:spreadsheet");
            writer.WriteAttributeString("xmlns", "html", null, "http://www.w3.org/TR/REC-html40");

            // <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
            writer.WriteStartElement("DocumentProperties", "urn:schemas-microsoft-com:office:office");

            // Dokumenteingeschaften schreiben
            writer.WriteElementString("Author", Environment.UserName);
            writer.WriteElementString("LastAuthor", Environment.UserName);
            writer.WriteElementString("Created", DateTime.Now.ToString("u") + "Z");
            writer.WriteElementString("Company", "Nils Holle");

            // </DocumentProperties>
            writer.WriteEndElement();

            // <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
            writer.WriteStartElement("ExcelWorkbook", "urn:schemas-microsoft-com:office:excel");

            // Arbeitsmappen-Einstellungen schreiben
            writer.WriteElementString("WindowHeight", "13170");
            writer.WriteElementString("WindowWidth", "17580");
            writer.WriteElementString("WindowTopX", "120");
            writer.WriteElementString("WindowTopY", "60");
            writer.WriteElementString("ProtectStructure", "False");
            writer.WriteElementString("ProtectWindows", "False");

            // </ExcelWorkbook>
            writer.WriteEndElement();

            // <Styles>
            writer.WriteStartElement("Styles");

            // <Style ss:ID="Default" ss:Name="Normal">
            writer.WriteStartElement("Style");
            writer.WriteAttributeString("ss", "ID", null, "Default");
            writer.WriteAttributeString("ss", "Name", null, "Normal");

            // <Alignment ss:Vertical="Bottom"/>
            writer.WriteStartElement("Alignment");
            writer.WriteAttributeString("ss", "Vertical", null, "Bottom");
            writer.WriteEndElement();

            // Verbleibende Sytle-Eigenschaften leer schreiben
            writer.WriteElementString("Borders", null);
            writer.WriteElementString("Font", null);
            writer.WriteElementString("Interior", null);
            writer.WriteElementString("NumberFormat", null);
            writer.WriteElementString("Protection", null);

            // </Style>
            writer.WriteEndElement();

            // </Styles>
            writer.WriteEndElement();

            // <Worksheet ss:Name="xxx">
            writer.WriteStartElement("Worksheet");
            writer.WriteAttributeString("ss", "Name", null, dataSource.TableName);

            // <Table ss:ExpandedColumnCount="2" ss:ExpandedRowCount="3" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="60">
            writer.WriteStartElement("Table");
            writer.WriteAttributeString("ss", "ExpandedColumnCount", null, dataSource.Columns.Count.ToString());
            writer.WriteAttributeString("ss", "ExpandedRowCount", null, dataSource.Rows.Count.ToString());
            writer.WriteAttributeString("x", "FullColumns", null, "1");
            writer.WriteAttributeString("x", "FullRows", null, "1");
            writer.WriteAttributeString("ss", "DefaultColumnWidth", null, "140");

            // Alle Zeilen der Datenquelle durchlaufen
            foreach (DataRow row in dataSource.Rows)
            {
                // <Row>
                writer.WriteStartElement("Row");

                // Alle Zellen der aktuellen Zeile durchlaufen
                foreach (object cellValue in row.ItemArray)
                {
                    // <Cell>
                    writer.WriteStartElement("Cell");

                    // <Data ss:Type="String">xxx</Data>
                    writer.WriteStartElement("Data");
                    writer.WriteAttributeString("ss", "Type", null, "String");

                    // Zelleninhakt schreiben
                    writer.WriteValue(cellValue);

                    // </Data>
                    writer.WriteEndElement();

                    // </Cell>
                    writer.WriteEndElement();
                }
                // </Row>
                writer.WriteEndElement();
            }
            // </Table>
            writer.WriteEndElement();

            // <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
            writer.WriteStartElement("WorksheetOptions", "urn:schemas-microsoft-com:office:excel");

            // Seiteneinstellungen schreiben
            writer.WriteStartElement("PageSetup");
            writer.WriteStartElement("Header");
            writer.WriteAttributeString("x", "Margin", null, "0.4921259845");
            writer.WriteEndElement();
            writer.WriteStartElement("Footer");
            writer.WriteAttributeString("x", "Margin", null, "0.4921259845");
            writer.WriteEndElement();
            writer.WriteStartElement("PageMargins");
            writer.WriteAttributeString("x", "Bottom", null, "0.984251969");
            writer.WriteAttributeString("x", "Left", null, "0.78740157499999996");
            writer.WriteAttributeString("x", "Right", null, "0.78740157499999996");
            writer.WriteAttributeString("x", "Top", null, "0.984251969");
            writer.WriteEndElement();
            writer.WriteEndElement();

            // <Selected/>
            writer.WriteElementString("Selected", null);

            // <Panes>
            writer.WriteStartElement("Panes");

            // <Pane>
            writer.WriteStartElement("Pane");

            // Bereichseigenschaften schreiben
            writer.WriteElementString("Number", "1");
            writer.WriteElementString("ActiveRow", "1");
            writer.WriteElementString("ActiveCol", "1");

            // </Pane>
            writer.WriteEndElement();

            // </Panes>
            writer.WriteEndElement();

            // <ProtectObjects>False</ProtectObjects>
            writer.WriteElementString("ProtectObjects", "False");

            // <ProtectScenarios>False</ProtectScenarios>
            writer.WriteElementString("ProtectScenarios", "False");

            // </WorksheetOptions>
            writer.WriteEndElement();

            // </Worksheet>
            writer.WriteEndElement();

            // </Workbook>
            writer.WriteEndElement();

            // Datei auf Festplatte schreiben
            writer.Flush();
            writer.Close();
        }

        private static void error()
        {
            Calc.Vogon.Error(Calc.Languages.Get("keyErrorNoData"));
        }

        private static void confirm()
        {
            Calc.Vogon.Info(Calc.Languages.Get("keyExcelSuccessfull"));
        }


        public static void Everything()
        {
            if (Calc.Vogon.TestGewicht() && Calc.Vogon.TestAussen() && Calc.Vogon.TestInnen())
            {
                // Create a new DataTable.
                DataTable table = new DataTable("Excel");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn column;
                DataRow row;

                // Create new DataColumn, set DataType, 
                // ColumnName and add to DataTable.    
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Name";
                column.ReadOnly = true;
                column.Unique = true;
                // Add the Column to the DataColumnCollection.
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Daten";
                column.AutoIncrement = false;
                column.ReadOnly = false;
                column.Unique = false;
                // Add the column to the table.
                table.Columns.Add(column);

                // Zeilen
                // Legierung
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLegierung");
                row["Daten"] = Calc.Vogon.CurrentData.Legierung;
                table.Rows.Add(row);

                // Spez. Gewicht
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyDichte");
                row["Daten"] = Calc.Vogon.CurrentData.SpezGewicht.ToString();
                table.Rows.Add(row);

                // Aussen
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyAussen");
                row["Daten"] = Calc.Vogon.CurrentData.Aussen.ToString();
                table.Rows.Add(row);

                // TolAussenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenPlus");
                row["Daten"] = Calc.Vogon.CurrentData.TolAussenPlus.ToString();
                table.Rows.Add(row);

                // TolAussenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenMinus");
                row["Daten"] = Calc.Vogon.CurrentData.TolAussenMinus.ToString();
                table.Rows.Add(row);

                // Innen
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyInnen");
                row["Daten"] = Calc.Vogon.CurrentData.Innen.ToString();
                table.Rows.Add(row);

                // TolInnenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenPlus");
                row["Daten"] = Calc.Vogon.CurrentData.TolInnenPlus.ToString();
                table.Rows.Add(row);

                // TolInnenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenMinus");
                row["Daten"] = Calc.Vogon.CurrentData.TolInnenMinus.ToString();
                table.Rows.Add(row);

                // Wand
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWand");
                row["Daten"] = Calc.Vogon.CurrentData.Wand.ToString();
                table.Rows.Add(row);

                // TolWandPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandPlus");
                row["Daten"] = Calc.Vogon.CurrentData.TolWandPlus.ToString();
                table.Rows.Add(row);

                // TolWandMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandMinus");
                row["Daten"] = Calc.Vogon.CurrentData.TolWandMinus.ToString();
                table.Rows.Add(row);

                // Meter pro Kg
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyMeterPerKg");
                row["Daten"] = Calc.Calculate.meterProKG();
                table.Rows.Add(row);

                // Kg pro Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyKgPerMeter");
                row["Daten"] = Calc.Calculate.kgProMeter();
                table.Rows.Add(row);

                // Stückgewicht
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWeightPerPiece");
                row["Daten"] = Calc.Calculate.stueckgewichtKgProStueck();
                table.Rows.Add(row);

                // Stücklänge
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLengthPerPiece");
                row["Daten"] = Calc.Vogon.CurrentData.Laenge;
                table.Rows.Add(row);

                // Liefermenge in Kg
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeKg");
                row["Daten"] = Calc.Vogon.CurrentData.LiefermengeKg;
                table.Rows.Add(row);

                // Liefermenge in Stk.
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeStk");
                row["Daten"] = Calc.Vogon.CurrentData.LiefermengeStk;
                table.Rows.Add(row);

                // Liefermenge in Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeMeter");
                row["Daten"] = Calc.Calculate.liefermengeMeter();
                table.Rows.Add(row);

                // Materialpreis
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyMaterialkosten");
                row["Daten"] = Calc.Vogon.CurrentData.Materialpreis;
                table.Rows.Add(row);

                // MaterialpreisEinheit
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyMaterialkostenEinheit");
                row["Daten"] = Calc.Vogon.CurrentData.MaterialpreisEinheit;
                table.Rows.Add(row);

                // Bearbeitungspreis
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyBearbeitungskosten");
                row["Daten"] = Calc.Vogon.CurrentData.Bearbeitungspreis;
                table.Rows.Add(row);

                // BearbeitungspreisEinheit
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyBearbeitungskostenEinheit");
                row["Daten"] = Calc.Vogon.CurrentData.BearbeitungspreisEinheit;
                table.Rows.Add(row);

                if (Calc.Vogon.TestMaterialpreis() && Calc.Vogon.TestBearbeitungspreis())
                {
                    // Kosten pro Stück
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyCostsPerPiece");
                    row["Daten"] = Calc.Calculate.kostenProStueck();
                    table.Rows.Add(row);

                    // Kosten gesamt
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyCosts");
                    row["Daten"] = Calc.Calculate.kostenGesamt();
                    table.Rows.Add(row);
                }

                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // save.Filter = "Xml-Dateien (*.xml)|*.xml";
                // save.Title = Calc.Languages.Get("keySaveFileAs");

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     exportDataTableToWorksheet(table, save.FileName);

                //     excelCurrentFilename = save.FileName;

                //     confirm();
                // }
            }
            else error();
        }

        public static void Everything(int oldSaved, int index)
        {
            if (Calc.Vogon.TestGewicht(oldSaved, index) && Calc.Vogon.TestAussen(oldSaved, index) && Calc.Vogon.TestInnen(oldSaved, index))
            {
                // Create a new DataTable.
                DataTable table = new DataTable("Excel");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn column;
                DataRow row;

                // Create new DataColumn, set DataType, 
                // ColumnName and add to DataTable.    
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Name";
                column.ReadOnly = true;
                column.Unique = true;
                // Add the Column to the DataColumnCollection.
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Daten";
                column.AutoIncrement = false;
                column.ReadOnly = false;
                column.Unique = false;
                // Add the column to the table.
                table.Columns.Add(column);

                // Zeilen
                // Legierung
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLegierung");
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Legierung;
                table.Rows.Add(row);

                // Spez. Gewicht
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyDichte");
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).SpezGewicht;
                table.Rows.Add(row);

                // Aussen
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyAussen");
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Aussen;
                table.Rows.Add(row);

                // TolAussenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenPlus");
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolAussenPlus.ToString();
                table.Rows.Add(row);

                // TolAussenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenMinus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolAussenMinus.ToString();
                table.Rows.Add(row);

                // Innen
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyInnen");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Innen;
                table.Rows.Add(row);

                // TolInnenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenPlus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolInnenPlus.ToString();
                table.Rows.Add(row);

                // TolInnenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenMinus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolInnenMinus.ToString();
                table.Rows.Add(row);

                // Wand
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWand");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Wand;
                table.Rows.Add(row);

                // TolWandPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandPlus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolWandPlus.ToString();
                table.Rows.Add(row);

                // TolWandMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandMinus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolWandMinus.ToString();
                table.Rows.Add(row);

                // Meter pro Kg
                row = table.NewRow();
                row["Name"] = "Meter pro Kg";
                row["Daten"] = Calc.Calculate.meterProKG(oldSaved, index);
                table.Rows.Add(row);

                // Kg pro Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyKgPerMeter");;
                row["Daten"] = Calc.Calculate.kgProMeter(oldSaved, index);
                table.Rows.Add(row);

                // Stückgewicht
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWeightPerPiece");;
                row["Daten"] = Calc.Calculate.stueckgewichtKgProStueck(oldSaved, index);
                table.Rows.Add(row);

                if (Calc.Vogon.TestLaenge(oldSaved, index))
                {
                    // Stücklänge
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyLengthPerPiece");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Laenge;
                    table.Rows.Add(row);

                    if (Calc.Vogon.TestLiefermengeKg(oldSaved, index) && Calc.Vogon.TestLiefermengeStk(oldSaved, index))
                    {
                        // Liefermenge in Kg
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyLiefermengeKg");;
                        row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).LiefermengeKg;
                        table.Rows.Add(row);

                        // Liefermenge in Stk.
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyLiefermengeStk");;
                        row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).LiefermengeStk;
                        table.Rows.Add(row);

                        // Liefermenge in Meter
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyLiefermengeMeter");;
                        row["Daten"] = Calc.Calculate.liefermengeMeter(oldSaved, index);
                        table.Rows.Add(row);
                    }
                }

                if (Calc.Vogon.TestMaterialpreis(oldSaved, index))
                {
                    // Materialpreis
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyMaterialkosten");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Materialpreis;
                    table.Rows.Add(row);

                    // MaterialpreisEinheit
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyMaterialkostenEinheit");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit;
                    table.Rows.Add(row);
                }

                if (Calc.Vogon.TestBearbeitungspreis(oldSaved, index))
                {
                    // Bearbeitungspreis
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyBearbeitungskosten");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Bearbeitungspreis;
                    table.Rows.Add(row);

                    // BearbeitungspreisEinheit
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyBearbeitungskostenEinheit");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit;
                    table.Rows.Add(row);

                    if (Calc.Vogon.TestMaterialpreis(oldSaved, index))
                    {
                        // Kosten pro Stück
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyCostsPerPiece");;
                        row["Daten"] = Calc.Calculate.kostenProStueck(oldSaved, index);
                        table.Rows.Add(row);

                        // Kosten gesamt
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyCosts");;
                        row["Daten"] = Calc.Calculate.kostenGesamt(oldSaved, index);
                        table.Rows.Add(row);
                    }
                }

                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // save.Filter = "Xml-Dateien (*.xml)|*.xml";
                // save.Title = Calc.Languages.Get("keySaveFileAs");;

                // excelCurrentFilename = save.FileName;

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     exportDataTableToWorksheet(table, save.FileName);

                //     excelCurrentFilename = save.FileName;

                //     confirm();
                // }
            }
        }

        public static void UserInput()
        {
            if (Calc.Vogon.TestGewicht() && Calc.Vogon.TestAussen() && Calc.Vogon.TestInnen())
            {
                // Create a new DataTable.
                DataTable table = new DataTable("Excel");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn column;
                DataRow row;

                // Create new DataColumn, set DataType, 
                // ColumnName and add to DataTable.    
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Name";
                column.ReadOnly = true;
                column.Unique = true;
                // Add the Column to the DataColumnCollection.
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Daten";
                column.AutoIncrement = false;
                column.ReadOnly = false;
                column.Unique = false;
                // Add the column to the table.
                table.Columns.Add(column);

                // Zeilen
                // Legierung
                row = table.NewRow();
                row["Name"] = "Legierung";
                row["Daten"] = Calc.Vogon.CurrentData.Legierung;
                table.Rows.Add(row);

                // Spez. Gewicht
                row = table.NewRow();
                row["Name"] = "Spez. Gewicht";
                row["Daten"] = Calc.Vogon.CurrentData.SpezGewicht.ToString();
                table.Rows.Add(row);

                // Aussen
                row = table.NewRow();
                row["Name"] = "Aussen";
                row["Daten"] = Calc.Vogon.CurrentData.Aussen.ToString();
                table.Rows.Add(row);

                // TolAussenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenPlus");;
                row["Daten"] = Calc.Vogon.CurrentData.TolAussenPlus.ToString();
                table.Rows.Add(row);

                // TolAussenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenMinus");;
                row["Daten"] = Calc.Vogon.CurrentData.TolAussenMinus.ToString();
                table.Rows.Add(row);

                // Innen
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyInnen");;
                row["Daten"] = Calc.Vogon.CurrentData.Innen.ToString();
                table.Rows.Add(row);

                // TolInnenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenPlus");;
                row["Daten"] = Calc.Vogon.CurrentData.TolInnenPlus.ToString();
                table.Rows.Add(row);

                // TolInnenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenMinus");;
                row["Daten"] = Calc.Vogon.CurrentData.TolInnenMinus.ToString();
                table.Rows.Add(row);

                // Wand
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWand");;
                row["Daten"] = Calc.Vogon.CurrentData.Wand.ToString();
                table.Rows.Add(row);

                // TolWandPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandPlus");;
                row["Daten"] = Calc.Vogon.CurrentData.TolWandPlus.ToString();
                table.Rows.Add(row);

                // TolWandMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandMinus");;
                row["Daten"] = Calc.Vogon.CurrentData.TolWandMinus.ToString();
                table.Rows.Add(row);

                // Stücklänge
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLengthPerPiece");;
                row["Daten"] = Calc.Vogon.CurrentData.Laenge;
                table.Rows.Add(row);

                // Liefermenge in Kg
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeKg");;
                row["Daten"] = Calc.Vogon.CurrentData.LiefermengeKg;
                table.Rows.Add(row);

                // Liefermenge in Stk.
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeStk");;
                row["Daten"] = Calc.Vogon.CurrentData.LiefermengeStk;
                table.Rows.Add(row);

                // Materialpreis
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyMaterialkosten");;
                row["Daten"] = Calc.Vogon.CurrentData.Materialpreis;
                table.Rows.Add(row);

                // MaterialpreisEinheit
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyMaterialkostenEinheit");;
                row["Daten"] = Calc.Vogon.CurrentData.MaterialpreisEinheit;
                table.Rows.Add(row);

                // Bearbeitungspreis
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyBearbeitungskosten");;
                row["Daten"] = Calc.Vogon.CurrentData.Bearbeitungspreis;
                table.Rows.Add(row);

                // BearbeitungspreisEinheit
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyBearbeitungskostenEinheit");;
                row["Daten"] = Calc.Vogon.CurrentData.BearbeitungspreisEinheit;
                table.Rows.Add(row);


                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // save.Filter = "Xml-Dateien (*.xml)|*.xml";
                // save.Title = Calc.Languages.Get("keySaveFileAs");;

                // excelCurrentFilename = save.FileName;

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     exportDataTableToWorksheet(table, save.FileName);

                //     excelCurrentFilename = save.FileName;

                //     confirm();
                // }
            }
            else error();
        }

        public static void UserInput(int oldSaved, int index)
        {
            if (Calc.Vogon.TestGewicht(oldSaved, index) && Calc.Vogon.TestAussen(oldSaved, index) && Calc.Vogon.TestInnen(oldSaved, index))
            {
                // Create a new DataTable.
                System.Data.DataTable table = new DataTable("Excel");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn column;
                DataRow row;

                // Create new DataColumn, set DataType, 
                // ColumnName and add to DataTable.    
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Name";
                column.ReadOnly = true;
                column.Unique = true;
                // Add the Column to the DataColumnCollection.
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Daten";
                column.AutoIncrement = false;
                column.ReadOnly = false;
                column.Unique = false;
                // Add the column to the table.
                table.Columns.Add(column);

                // Zeilen
                // Legierung
                row = table.NewRow();
                row["Name"] = "Legierung";
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Legierung;
                table.Rows.Add(row);

                // Spez. Gewicht
                row = table.NewRow();
                row["Name"] = "Spez. Gewicht";
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).SpezGewicht;
                table.Rows.Add(row);

                // Aussen
                row = table.NewRow();
                row["Name"] = "Aussen";
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Aussen;
                table.Rows.Add(row);

                // TolAussenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenPlus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolAussenPlus.ToString();
                table.Rows.Add(row);

                // TolAussenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolAussenMinus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolAussenMinus.ToString();
                table.Rows.Add(row);

                // Innen
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyInnen");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Innen;
                table.Rows.Add(row);

                // TolInnenPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenPlus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolInnenPlus.ToString();
                table.Rows.Add(row);

                // TolInnenMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolInnenMinus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolInnenMinus.ToString();
                table.Rows.Add(row);

                // Wand
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWand");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Wand;
                table.Rows.Add(row);

                // TolWandPlus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandPlus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolWandPlus.ToString();
                table.Rows.Add(row);

                // TolWandMinus
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyTolWandMinus");;
                row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).TolWandMinus.ToString();
                table.Rows.Add(row);

                if (Calc.Vogon.TestLaenge(oldSaved, index))
                {
                    // Stücklänge
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyLengthPerPiece");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Laenge;
                    table.Rows.Add(row);

                    if (Calc.Vogon.TestLiefermengeKg(oldSaved, index) && Calc.Vogon.TestLiefermengeStk(oldSaved, index))
                    {
                        // Liefermenge in Kg
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyLiefermengeKg");;
                        row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).LiefermengeKg;
                        table.Rows.Add(row);

                        // Liefermenge in Stk.
                        row = table.NewRow();
                        row["Name"] = Calc.Languages.Get("keyLiefermengeStk");;
                        row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).LiefermengeStk;
                        table.Rows.Add(row);
                    }
                }

                if (Calc.Vogon.TestMaterialpreis(oldSaved, index))
                {
                    // Materialpreis
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyMaterialkosten");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Materialpreis;
                    table.Rows.Add(row);

                    // MaterialpreisEinheit
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyMaterialkostenEinheit");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).MaterialpreisEinheit;
                    table.Rows.Add(row);
                }

                if (Calc.Vogon.TestBearbeitungspreis(oldSaved, index))
                {
                    // Bearbeitungspreis
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyBearbeitungskosten");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).Bearbeitungspreis;
                    table.Rows.Add(row);

                    // BearbeitungspreisEinheit
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyBearbeitungskostenEinheit");;
                    row["Daten"] = Calc.Vogon.Data.Get(oldSaved, index).BearbeitungspreisEinheit;
                    table.Rows.Add(row);
                }

                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // save.Filter = "Xml-Dateien (*.xml)|*.xml";
                // save.Title = Calc.Languages.Get("keySaveFileAs");;

                // excelCurrentFilename = save.FileName;

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     exportDataTableToWorksheet(table, save.FileName);

                //     excelCurrentFilename = save.FileName;

                //     confirm();
                // }
            }
        }

        public static void CalculatedData()
        {
            if (Calc.Vogon.TestGewicht() && Calc.Vogon.TestAussen() && Calc.Vogon.TestInnen())
            {
                // Create a new DataTable.
                System.Data.DataTable table = new DataTable("Excel");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn column;
                DataRow row;

                // Create new DataColumn, set DataType, 
                // ColumnName and add to DataTable.    
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Name";
                column.ReadOnly = true;
                column.Unique = true;
                // Add the Column to the DataColumnCollection.
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Daten";
                column.AutoIncrement = false;
                column.ReadOnly = false;
                column.Unique = false;
                // Add the column to the table.
                table.Columns.Add(column);

                // Zeilen
                // Meter pro Kg
                row = table.NewRow();
                row["Name"] = "Meter pro Kg";
                row["Daten"] = Calc.Calculate.meterProKG();
                table.Rows.Add(row);

                // Kg pro Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyKgPerMeter");;
                row["Daten"] = Calc.Calculate.kgProMeter();
                table.Rows.Add(row);

                // Stückgewicht
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWeightPerPiece");;
                row["Daten"] = Calc.Calculate.stueckgewichtKgProStueck();
                table.Rows.Add(row);

                // Liefermenge in Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeMeter");;
                row["Daten"] = Calc.Calculate.liefermengeMeter();
                table.Rows.Add(row);

                if (Calc.Vogon.TestMaterialpreis() && Calc.Vogon.TestBearbeitungspreis())
                {
                    // Kosten pro Stück
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyCostsPerPiece");;
                    row["Daten"] = Calc.Calculate.kostenProStueck();
                    table.Rows.Add(row);

                    // Kosten gesamt
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyCosts");;
                    row["Daten"] = Calc.Calculate.kostenGesamt();
                    table.Rows.Add(row);
                }

                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // save.Filter = "Xml-Dateien (*.xml)|*.xml";
                // save.Title = Calc.Languages.Get("keySaveFileAs");;

                // excelCurrentFilename = save.FileName;

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     exportDataTableToWorksheet(table, save.FileName);

                //     excelCurrentFilename = save.FileName;

                //     confirm();
                // }
            }
            else error();
        }

        public static void CalculatedData(int oldSaved, int index)
        {
            if (Calc.Vogon.TestGewicht(oldSaved, index) && Calc.Vogon.TestAussen(oldSaved, index) && Calc.Vogon.TestInnen(oldSaved, index))
            {
                // Create a new DataTable.
                System.Data.DataTable table = new DataTable("Excel");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn column;
                DataRow row;

                // Create new DataColumn, set DataType, 
                // ColumnName and add to DataTable.    
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Name";
                column.ReadOnly = true;
                column.Unique = true;
                // Add the Column to the DataColumnCollection.
                table.Columns.Add(column);

                // Create second column.
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Daten";
                column.AutoIncrement = false;
                column.ReadOnly = false;
                column.Unique = false;
                // Add the column to the table.
                table.Columns.Add(column);

                // Zeilen
                // Meter pro Kg
                row = table.NewRow();
                row["Name"] = "Meter pro Kg";
                row["Daten"] = Calc.Calculate.meterProKG(oldSaved, index);
                table.Rows.Add(row);

                // Kg pro Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyKgPerMeter");;
                row["Daten"] = Calc.Calculate.kgProMeter(oldSaved, index);
                table.Rows.Add(row);

                // Stückgewicht
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyWeightPerPiece");;
                row["Daten"] = Calc.Calculate.stueckgewichtKgProStueck(oldSaved, index);
                table.Rows.Add(row);

                // Liefermenge in Meter
                row = table.NewRow();
                row["Name"] = Calc.Languages.Get("keyLiefermengeMeter");;
                row["Daten"] = Calc.Calculate.liefermengeMeter(oldSaved, index);
                table.Rows.Add(row);

                if (Calc.Vogon.TestMaterialpreis(oldSaved, index) && Calc.Vogon.TestBearbeitungspreis(oldSaved, index))
                {
                    // Kosten pro Stück
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyCostsPerPiece");;
                    row["Daten"] = Calc.Calculate.kostenProStueck(oldSaved, index);
                    table.Rows.Add(row);

                    // Kosten gesamt
                    row = table.NewRow();
                    row["Name"] = Calc.Languages.Get("keyCosts");;
                    row["Daten"] = Calc.Calculate.kostenGesamt(oldSaved, index);
                    table.Rows.Add(row);
                }

                // SaveFileDialog save = new SaveFileDialog();
                // save.InitialDirectory = "C:\\";
                // save.Filter = "Xml-Dateien (*.xml)|*.xml";
                // save.Title = Calc.Languages.Get("keySaveFileAs");;

                // excelCurrentFilename = save.FileName;

                // if (save.ShowDialog() == DialogResult.OK)
                // {
                //     exportDataTableToWorksheet(table, save.FileName);

                //     excelCurrentFilename = save.FileName;

                //     confirm();
                // }
            }
        }
    }
}
