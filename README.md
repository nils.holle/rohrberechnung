# Rohrberechnung

## TL;DR

Go here: [rohrberechnung.de](https://www.rohrberechnung.de)

## Introduction

Open Source Software zur Berechnung von Rohrgewichten, Rohrmaßen und Liefermengen. Viele nützliche Features, die jedem den Alltag erleichtern, der in irgendeiner Weise, sei es als Kunde, Techniker oder Verkäufer, mit Rohren zu tun haben. Die berechneten Daten lassen sich direkt als Mail versenden, drucken, für Excel exportieren und in die Zwischenablage kopieren. Auch in Englisch und Französisch verfügbar!
